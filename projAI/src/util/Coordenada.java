package util;

import java.util.Locale;
import java.util.Random;

public class Coordenada {
	private double x;
	private double y;

	public Coordenada(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Coordenada() {
		this.x = getRandom();
		this.y = getRandom();
	}

	public Coordenada(Coordenada c) {
		this.x = c.getX();
		this.y = c.getY();
	}

	public Coordenada(String c) {
		String[] temp = c.split(":");
		this.x = Double.parseDouble(temp[0]);
		this.y = Double.parseDouble(temp[1]);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	private double getRandom() {
		double max = 500;
		Random rn = new Random();
		return rn.nextDouble() * max;

	}

	@Override
	public String toString() {
		return String.format(Locale.US,"%.1f,%.1f", x, y);
	}

	public String serialize() {
		return String.format(Locale.US,"%f:%f", x, y);
	}

	public double distancia_a_coordenada(Coordenada c2) {
		return Math.sqrt(Math.pow(x - c2.getX(), 2) + Math.pow(y - c2.getY(), 2));
	}
}
