package util;

import java.util.Locale;

public class Estacao {

	private Coordenada coordenada;
	private double raio;
	private double incentivo;
	private int nBikes;
	private int capacidade;
	
	public Estacao(Coordenada coordenada, double raio, double incentivo, int nBikes,int capacidade) {
		this.coordenada = coordenada;
		this.raio = raio;
		this.incentivo = incentivo;
		this.nBikes=nBikes;
		this.capacidade=capacidade;
	}
	

	public Estacao(String serialized) {

		String[] parser = serialized.split(";");
		this.coordenada = new Coordenada(parser[0]);
		this.raio = Double.parseDouble(parser[1]);
		this.incentivo = Double.parseDouble(parser[2]);
		this.nBikes = Integer.parseInt(parser[3]);
		this.capacidade = Integer.parseInt(parser[4]);
	}

	public String serialize() {
		return String.format(Locale.US,"%s;%.3f;%.3f;%d;%d", coordenada.serialize(), raio, incentivo,nBikes,capacidade);
	}

	public Coordenada getCoordenada() {
		return coordenada;
	}

	public double getRatio() {
		return (double) nBikes/capacidade;
	}
	public double getIncentivo() {
		return incentivo;
	}

	public double getRaio() {
		return raio;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public int getnBikes() {
		return nBikes;
	}

}
