package main;

import jade.core.Runtime;

import java.awt.Dimension;

import gui.Dimensions;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

/**
 * 
 */

public class MainContainer {

	Runtime rt;
	ContainerController container;

	public void initMainContainerInPlatform(String host, String port, String containerName) {

		// Get the JADE runtime interface (singleton)
		this.rt = Runtime.instance();

		// Create a Profile, where the launch arguments are stored
		Profile prof = new ProfileImpl();
		prof.setParameter(Profile.CONTAINER_NAME, containerName);
		prof.setParameter(Profile.MAIN_HOST, host);
		prof.setParameter(Profile.MAIN_PORT, port);
		prof.setParameter(Profile.MAIN, "true");
		prof.setParameter(Profile.GUI, "true");

		// create a main agent container
		this.container = rt.createMainContainer(prof);
		rt.setCloseVM(true);
	}

	public void startAgentInPlatform(String name, String classpath) {
		try {
			AgentController ac = container.createNewAgent(name, classpath, new Object[0]);
			ac.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void startAgentInPlatform(String name, String classpath, Object[] args) {
		try {
			AgentController ac = container.createNewAgent(name, classpath, args);
			ac.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		MainContainer a = new MainContainer();

		a.initMainContainerInPlatform("localhost", "9888", "MainContainer");
		
		
		/*//predefinir estacoes
		Object[] arg = new Object[5];
		arg[0] = "20";
		arg[1] = "20";
		arg[2] = "10";
		arg[3] = "3";
		arg[4] = "6";
		//coluna 1
		for(int i=0;i<3;i++) {
			arg[1] = ((Double)arg[1])+Dimensions.GRID_HEIGHT/4+"";
			a.startAgentInPlatform("StationAgent"+i, "agentes.StationAgent", arg);
		}
		*/

		
		
		int nStations=6;
		for(int i=0;i<nStations;i++) {
			a.startAgentInPlatform("StationAgent"+i, "agentes.StationAgent");
		}
		
		a.startAgentInPlatform("InterfaceAgent", "agentes.InterfaceAgent");
		
		int COSTUMERS_POR_INTERVALO=7;
		int INTERVALO_TEMPO = 5; //em segundos
		
		int nCostumer = 1;
		while (true) {
			for (int i = 0; i < COSTUMERS_POR_INTERVALO; i++) {
				a.startAgentInPlatform("CostumerAgent" + (nCostumer++), "agentes.CostumerAgent");
			}
			try {
				Thread.sleep(INTERVALO_TEMPO*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}