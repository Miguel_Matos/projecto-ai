(deftemplate Costumer
        (slot posX)
        (slot posY)
        (slot distFactor)
        (slot incentFactor) 
        (slot threshold)
)

(deftemplate Station
	(slot id)
	(slot posX)
	(slot posY)
	(slot raio)
	(slot incentivo)
)


(deftemplate ACLMessage 
	(slot communicative-act) 
        (slot sender)
        (multislot receiver) 
	(slot reply-with)
        (slot in-reply-to)
        (slot envelope) 
	(slot conversation-id)
        (slot protocol)
        (slot language) 
	(slot ontology)
        (slot content)
        (slot encoding) 
	(multislot reply-to)
        (slot reply-by) 
)

(defrule checkArea
       	?s<-(Station (id ?i) (posX ?x) (posY ?y) (raio ?r) (incentivo ?in))
       	(Costumer (posX ?px) (posY ?py) (distFactor ?df) (incentFactor ?if) (threshold ?th))
 =>  	
        (bind ?dist (sqrt (+ (** (- ?px ?x) 2) (** (- ?py ?y) 2) )))
        ;(printout t "****************************" crlf)
         ;(printout t "Threshold: " ?th crlf)
         ; (printout t "incentive: " ?if crlf)
        ;(printout t "Station : "  ?i " X: " ?x " Y: " ?y crlf)
        ;(printout t "Distancia : " ?dist crlf)
        (if  (and (<= ?dist ?r) (>= ?in ?th))
   		then 
   			(bind ?score (+ (/ 1 (* ?df ?dist) ) (* ?if ?in) ))
   			;(printout t "Score : " ?score crlf)
   			(bind ?m (assert (ACLMessage (communicative-act 11) (receiver ?i) (content ?score))))
        (send ?m)
        (retract ?m)
   		else ( 
   			if (and (> ?dist ?r) (>= ?in ?th))
   				then 
   					(bind ?score (+ (/ 1 (* ?df ?dist) ) (* ?if ?in) ))
   					;(printout t "Score : " ?score crlf)
   					(bind ?m (assert (ACLMessage (communicative-act 10) (receiver ?i) (content ?score))))
        		(send ?m)
        		(retract ?m)))
        (retract ?s)
)


		

