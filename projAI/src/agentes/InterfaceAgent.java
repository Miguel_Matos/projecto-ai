package agentes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import gui.BarChart;
import gui.Gui;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import util.Coordenada;
import util.Estacao;

public class InterfaceAgent extends Agent {
	private Map<String, Coordenada> costumers = new HashMap<>();
	private Map<String, Estacao> estacoes = new HashMap<>();

	private int receivedMessages;
	private int sentMessages;

	private int TEMPO_ACTUALIZACAO = 500;
	private Gui mapa;
	private BarChart chart;

	@Override
	protected void setup() {

		receivedMessages = 0;
		sentMessages = 0;

		this.addBehaviour(new RequestInfo(this, TEMPO_ACTUALIZACAO));
		this.addBehaviour(new RecepcaoMensagens());
		
		mapa = new Gui();
		mapa.start();
		chart=null;
	}

	private class RecepcaoMensagens extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				receivedMessages++;

				if (msg.getProtocol().equals("Station")) {
					Estacao e = new Estacao(msg.getContent());
					estacoes.put(msg.getSender().getLocalName(), e);
				} else {
					Coordenada c = new Coordenada(msg.getContent());
					costumers.put(msg.getSender().getLocalName(), c);
				}

				if (receivedMessages == sentMessages) {
					Map<String, Coordenada> cos = new HashMap<>();
					Map<String, Estacao> est = new HashMap<>();
					for(Entry<String,Coordenada> entry:costumers.entrySet()) {
						cos.put(entry.getKey(),entry.getValue());
					}
					for(Entry<String,Estacao> entry:estacoes.entrySet()) {
						est.put(entry.getKey(),entry.getValue());
					}
					mapa.update(cos,est);
					if(chart==null) chart = new BarChart("Stations Cargo","Stations Cargo", est,TEMPO_ACTUALIZACAO);
					else chart.execute(est);
					costumers.clear();
					estacoes.clear();
				}
			} else {
				block();
			}
		}
	}

	private class RequestInfo extends TickerBehaviour {

		public RequestInfo(Agent a, long period) {
			super(a, period);
		}

		@Override
		protected void onTick() {
			DFAgentDescription df = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			df.addServices(sd);

			try {
				DFAgentDescription[] result = DFService.search(myAgent, df);
				sentMessages = result.length;
				receivedMessages = 0;

				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.setProtocol("Interface");

				for (int i = 0; i < result.length; i++) {
					msg.addReceiver(result[i].getName());
				}

				send(msg);
			} catch (FIPAException e) {
				e.printStackTrace();
			}
		}

	}
	
	
}
