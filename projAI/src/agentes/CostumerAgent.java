package agentes;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import util.Coordenada;
import util.Estacao;
import jess.Context;
import jess.Fact;
import jess.Filter;
import jess.FilteringIterator;
import jess.Funcall;
import jess.JessException;
import jess.Rete;
import jess.Value;
import jess.ValueVector;

public class CostumerAgent extends Agent {

	private Coordenada posInicial;
	private Coordenada posFinal;
	private Coordenada posActual;
	private Coordenada posDestino; // estacao destino
	private boolean withBike;
	private Map<AID, Estacao> estacoes;

	private double incentFactor;
	private double distFactor;

	private Rete engine;

	private double maxScore;
	private AID stationDestiny;
	private int stationStatus; // 0 outside , 1 inside
	private Map<String, AID> AIDInfo;
	private boolean end;
	private AID excludeStation;
	private long UPDATE_MOVIMENTO = 500;
	private double toleranciaDistancias = 0;
	private double threshold;

	public CostumerAgent() {

		do {
			posInicial = new Coordenada();
			posFinal = new Coordenada();
		} while (posInicial.distancia_a_coordenada(posFinal) < 100); // garantir que o percurso aleat�rio � longo

		posActual = new Coordenada(posInicial.getX(), posInicial.getY());
		withBike = false;
		estacoes = new HashMap<>();

		Random rn = new Random();
		incentFactor = rn.nextDouble() ;
		distFactor = rn.nextDouble();
		threshold= 0.10+0.10*rn.nextDouble();

		posDestino = new Coordenada(posFinal);
		maxScore = 0;
		stationStatus = 0;
		stationDestiny = null;
		AIDInfo = new HashMap<>();
		end = false;
		excludeStation = null;
	}

	
	@Override
	public void setup() {
		engine = new Rete();
		try {
			engine.addUserfunction(new JessSend());
			engine.batch("agentes/CostumerAgent.clp");
			engine.reset();
			engine.executeCommand("(assert ( Costumer ( posX " + posActual.getX() + ") ( posY " + posActual.getY()
					+ ") ( distFactor " + distFactor + ") ( incentFactor " + incentFactor + ") (threshold "+threshold+")))");
		} catch (JessException ex) {
			ex.printStackTrace();
		}

		register();
		this.addBehaviour(new AskStationInfo());
		this.addBehaviour(new Movement(this, UPDATE_MOVIMENTO));
		this.addBehaviour(new TransmitCostumerStatus());
	}

	private class TransmitCostumerStatus extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol("Interface"));
			if (msg != null) {

				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				reply.setProtocol("Costumer");
				reply.setContent(posActual.serialize());
				send(reply);

			} else {
				block();
			}
		}
	}
	

	private void register() {
		DFAgentDescription df = new DFAgentDescription();
		df.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setType("Costumer");
		sd.setName(getLocalName());

		df.addServices(sd);

		try {
			DFService.register(this, df);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	

	private class GetBike extends Behaviour {
		private int step = 1;
		private AID station;

		public GetBike(AID station) {
			this.station = station;
		}

		@Override
		public void action() {
			ACLMessage msg;
			switch (step) {
			case 1:
				msg = new ACLMessage(ACLMessage.REQUEST);
				msg.setProtocol("GetBike");
				msg.addReceiver(this.station);
				msg.setContent("Request bike");

				send(msg);
				step++;
				break;
			case 2:
				msg = receive(MessageTemplate.MatchProtocol("GetBike"));

				if (msg != null) {
					if (msg.getPerformative() == ACLMessage.CONFIRM) {
						posInicial = new Coordenada(posDestino);
						withBike = true;

						maxScore = 0;
						stationStatus = 0;
						stationDestiny = null;
						posDestino = new Coordenada(posFinal);

					} else if (msg.getPerformative() == ACLMessage.CANCEL) {
						excludeStation = msg.getSender();
						maxScore = 0;
						stationStatus = 0;
						stationDestiny = null;
						addBehaviour(new AskStationInfo());
					}

					step++;
				} else {
					block();
				}

				break;
			default:
				break;
			}

		}

		@Override
		public boolean done() {
			return step > 2;
		}

	}
	

	private class DeliverBike extends Behaviour {
		private int step = 1;
		private AID station;

		public DeliverBike(AID station) {
			this.station = station;
		}

		@Override
		public void action() {
			ACLMessage msg;
			switch (step) {
			case 1:
				msg = new ACLMessage(ACLMessage.CONFIRM);
				msg.setProtocol("DeliverBike");
				msg.setContent("Deliver bike");
				msg.addReceiver(this.station);
				send(msg);
				step++;
				break;
			case 2:
				msg = receive(MessageTemplate.MatchProtocol("DeliverBike"));

				if (msg != null) {
					if (msg.getPerformative() == ACLMessage.CONFIRM) {
						withBike = false;
						end = true;

						maxScore = 0;
						stationStatus = 0;
						stationDestiny = null;
						posDestino = new Coordenada(posFinal);

					} else if (msg.getPerformative() == ACLMessage.CANCEL) {
						excludeStation = msg.getSender();
						maxScore = 0;
						stationStatus = 0;
						stationDestiny = null;
						addBehaviour(new AskStationInfo());
					}

					step++;
				} else {
					block();
				}

				break;
			default:
				break;
			}

		}

		@Override
		public boolean done() {
			return step > 2;
		}

	}

	// 1� step: envio das mensagens �s estacoes
	// 2� step: recepcao de todas as respostas
	private class AskStationInfo extends Behaviour {

		private int step = 1;
		private int sentMessages = 0;
		private int receivedMessages = 0;

		@Override
		public void action() {

			switch (step) {
			case 1:
				DFAgentDescription df = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Station");
				df.addServices(sd);

				try {
					DFAgentDescription[] result = DFService.search(myAgent, df);
					sentMessages = result.length;

					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

					if (!withBike) {
						msg.setContent("search");
					} else {
						msg.setContent("deliver");
					}

					for (int i = 0; i < result.length; i++) {
						if (!(excludeStation != null && excludeStation.getName().equals(result[i].getName().getName())))
							msg.addReceiver(result[i].getName());
					}

					send(msg);
				} catch (FIPAException e) {
					e.printStackTrace();
				}

				step++;
				break;
			case 2:
				ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));

				if (msg != null) {
					try {
						Estacao estacaoInfo = new Estacao(msg.getContent());

						AID station = msg.getSender();
						estacoes.put(station, estacaoInfo);
						engine.executeCommand("(assert (Station (id " + msg.getSender().getLocalName() + ") (posX "
								+ estacaoInfo.getCoordenada().getX() + ") (posY " + estacaoInfo.getCoordenada().getY()
								+ ") (raio " + estacaoInfo.getRaio() + ") (incentivo " + estacaoInfo.getIncentivo()
								+ ")))");
						engine.run();

					} catch (JessException ex) {
						ex.printStackTrace();
					}
					receivedMessages++;

				} else {
					block();
				}

				if (receivedMessages == sentMessages) {
					step++;
				}
				break;
			default:
				break;
			}
		}

		@Override
		public boolean done() {
			return step > 2;
		}

	}

	private class Movement extends TickerBehaviour {

		public Movement(Agent a, long period) {
			super(a, period);
		}

		private double calculateAngle(double x1, double y1, double x2, double y2) {
			double angle = Math.atan2(y1 - y2, x1 - x2);
			return angle;
		}

		private Coordenada movePoint() {
			double angle = calculateAngle(posDestino.getX(), posDestino.getY(), posActual.getX(), posActual.getY());
			double d;
			if (!withBike) {
				d = 20;
			} else {
				d = 40;
			}
			toleranciaDistancias = d / 2.0;
			double x = posActual.getX() + d * Math.cos(angle);
			double y = posActual.getY() + d * Math.sin(angle);
			return new Coordenada(x, y);
		}

		private void updateCostumerFact() {
			FilteringIterator it = new FilteringIterator(engine.listFacts(), new Filter.ByModule("MAIN"));
			it.next();
			while (it.hasNext()) {
				Fact f = (Fact) it.next();
				try {
					engine.retract(f);
				} catch (JessException e) {
					e.printStackTrace();
				}
			}

			try {
				engine.executeCommand("(assert ( Costumer ( posX " + posActual.getX() + ") ( posY " + posActual.getY() +") ( distFactor " + distFactor + ") ( incentFactor " + incentFactor + ") (threshold "+threshold+")))");
			} catch (JessException e1) {
				e1.printStackTrace();
			}
		}

		@Override
		protected void onTick() {
			posActual = movePoint();
			// System.out.println("Destiny: " + posDestino);

			/*
			 * if (excludeStation == null) System.out.println("Exclude: None "); else
			 * System.out.println("Exclude: " + excludeStation.getName());
			 */
			
			double dist = posActual.distancia_a_coordenada(posDestino);
			double distTotal = posInicial.distancia_a_coordenada(posFinal);
			double distPercorrida = posActual.distancia_a_coordenada(posInicial);
			double distEnd = posActual.distancia_a_coordenada(posFinal);

			updateCostumerFact();

			if (distEnd < toleranciaDistancias && end == true) { // Reached final destination
				System.out.println("Agent Shutting Down..");
				DFAgentDescription dfd = new DFAgentDescription();
				dfd.setName(getAID());
				try {
					DFService.deregister(myAgent, dfd);
				} catch (FIPAException e) {
				}
				myAgent.doDelete();
			}

			if (withBike == false && end == false && distEnd < toleranciaDistancias) { 
				// If too close to destination
				end = true;
				posDestino = new Coordenada(posFinal);
			}

			if (distPercorrida >= 0.75 * distTotal && stationStatus == 0 && end == false) { // Reached 3/4 of distance
				System.out.println("Searching station");
				maxScore = 0;
				stationStatus = 0;
				stationDestiny = null;

				addBehaviour(new AskStationInfo());
			}

			if (dist < toleranciaDistancias && end == false) { // Reached a station
				if (!withBike)
					addBehaviour(new GetBike(stationDestiny));
				else
					addBehaviour(new DeliverBike(stationDestiny));
			}

		}
	}

	private class JessSend implements jess.Userfunction {

		@Override
		public String getName() {
			return ("send");
		}

		@Override
		public Value call(ValueVector vv, Context cntxt) throws JessException {
			Fact f = vv.get(1).factValue(cntxt);
			factRead(cntxt, f);

			return Funcall.TRUE;
		}

	}

	public AID getAIDfromInfo(String a) {
		AID aid = (AID) AIDInfo.get(a);

		if (aid == null) {
			return new AID(a, AID.ISLOCALNAME);
		}

		return aid;
	}

	/*
	 * Transform the Jess fact to a ACLMessage
	 */
	private void factRead(Context context, ValueVector vv) {

		try {
			int performative = Integer.parseInt(vv.get(0).stringValue(context));
			ArrayList<AID> receivers = getInfo(context, vv.get(2).listValue(context));
			String content = (vv.get(10).stringValue(context));
			if (performative == 11 && end == false) {
				double score = Double.parseDouble(content);
				if (score > maxScore) {
					maxScore = score;
					stationDestiny = receivers.get(0);
					posDestino = estacoes.get(stationDestiny).getCoordenada();
					stationStatus = 1;
				}
			} else if (performative == 10 && end == false) {
				double score = Double.parseDouble(content);
				if (score > maxScore && stationStatus==0) {
					maxScore = score;
					stationDestiny = receivers.get(0);
					posDestino = estacoes.get(stationDestiny).getCoordenada();
				}
			}

		} catch (JessException ex) {
			Logger.getLogger(CostumerAgent.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public ArrayList<AID> getInfo(Context ctx, ValueVector vv) {
		ArrayList<AID> aux = new ArrayList<>();

		for (int i = 0; i < vv.size(); i++) {
			try {
				aux.add(getAIDfromInfo(vv.get(i).stringValue(ctx)));
			} catch (JessException ex) {
				Logger.getLogger(CostumerAgent.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		return aux;
	}

}
