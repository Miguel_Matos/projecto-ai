package agentes;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import util.Coordenada;
import util.Estacao;

public class StationAgent extends Agent {

	private Coordenada coordenada;
	private double raio;
	private int capacidadeMax;
	private int nBicicletas;

	@Override
	public void setup() {
		
		coordenada = new Coordenada();
		raio = 100;
		capacidadeMax = 6;
		nBicicletas = (int) (capacidadeMax*0.4+(capacidadeMax-(capacidadeMax*0.4))*Math.random());
		/*
		Object[] args = getArguments();
		double x = Double.parseDouble(args[0].toString());
		double y = Double.parseDouble(args[1].toString());
		coordenada = new Coordenada(x, y);
		raio = Double.parseDouble(args[2].toString());
		capacidadeMax = Integer.parseInt(args[4].toString());
		nBicicletas = Integer.parseInt(args[3].toString());
		 */
		
		register();
		this.addBehaviour(new AcceptBike());
		this.addBehaviour(new GiveBike());
		this.addBehaviour(new TransmitStationInfo());
		this.addBehaviour(new TransmitStationStatus());
	}

	private void register() {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());

		ServiceDescription sd = new ServiceDescription();
		sd.setType("Station");
		sd.setName(getLocalName());

		dfd.addServices(sd);

		try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}

	private double getIncentivoEntrega() {
		double ratio = (double)nBicicletas / capacidadeMax;
		double x;
		if (ratio >= 0.9) {
			x = -0.15;
		} else if (ratio >= 0.6) {
			x = 0.2;
		} else if (ratio >= 0.3) {
			x = 0.4;
		} else if (ratio >= 0.1) {
			x = 0.7;
		} else {
			x = 1;
		}

		double incentivo = 0.1 + 0.65 * x;
		return incentivo;
	}

	private double getIncentivoProcura() {
		double ratio = (double) nBicicletas / capacidadeMax;
		double x;
		if (ratio >= 0.9) {
			x = 1;
		} else if (ratio >= 0.6) {
			x = 0.7;
		} else if (ratio >= 0.3) {
			x = 0.4;
		} else if (ratio >= 0.1) {
			x = 0.2;
		} else {
			x = -0.15;
		}

		double incentivo = 0.1 + 0.65 * x;
		return incentivo;
	}

	private class AcceptBike extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol("DeliverBike"));
			
			if (msg != null) {

				ACLMessage reply = msg.createReply();
				if (nBicicletas < capacidadeMax) {
					nBicicletas++;
					reply.setPerformative(ACLMessage.CONFIRM);
					reply.setContent("Delivery successful");

					System.out.println("Entrega com sucesso");
				} else {
					reply.setPerformative(ACLMessage.CANCEL);
					reply.setContent("Delivery was a failure");
					System.out.println("Entrega sem sucesso");
				}
				send(reply);

			} else {
				block();
			}
		}
	}

	private class GiveBike extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol("GetBike"));
			
			if (msg != null) {

				ACLMessage reply = msg.createReply();
				if (nBicicletas > 0) {
					nBicicletas--;
					reply.setPerformative(ACLMessage.CONFIRM);
					reply.setContent("Request successful");

					System.out.println("Bicicleta Alugada");
				} else {
					reply.setPerformative(ACLMessage.CANCEL);
					reply.setContent("Request was a failure");

					System.out.println("Falha no alugamento de bicicleta");
				}
				send(reply);

			} else {
				block();
			}
		}
	}

	private class TransmitStationInfo extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
			if (msg != null) {

				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);

				double incentivo;
				if (msg.getContent().equals("search")) {
					incentivo = getIncentivoProcura();
				} else {
					incentivo = getIncentivoEntrega();
				}

				Estacao estacaoInfo = new Estacao(coordenada, raio, incentivo,nBicicletas,capacidadeMax);
				reply.setContent(estacaoInfo.serialize());
				send(reply);

			} else {
				block();
			}
		}
	}

	private class TransmitStationStatus extends CyclicBehaviour {

		@Override
		public void action() {
			ACLMessage msg = receive(MessageTemplate.MatchProtocol("Interface"));
			if (msg != null) {

				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				reply.setProtocol("Station");
				Estacao estacaoInfo=new Estacao(coordenada, raio, 0,nBicicletas,capacidadeMax);
				reply.setContent(estacaoInfo.serialize());
				send(reply);

			} else {
				block();
			}
		}
	}

}