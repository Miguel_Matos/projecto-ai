package gui;

import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import javafx.application.Platform;
import util.Coordenada;
import util.Estacao;

public class BarChart extends ApplicationFrame implements Runnable {
	private static int tempoActualizacao = 1000;
	private JFreeChart barChart;
	private DefaultCategoryDataset model = new DefaultCategoryDataset();

	public BarChart(String applicationTitle, String chartTitle, Map<String, Estacao> est, int tempo) {
		super(applicationTitle);
		tempoActualizacao = tempo;
		createDataset(est);
		barChart = ChartFactory.createBarChart(chartTitle, "Stations", "Bikes", model, PlotOrientation.VERTICAL, true,
				true, false);
		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
		setContentPane(chartPanel);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void createDataset(Map<String, Estacao> est) {
		final String nbicicletas = "No. Bikes";
		final String capacidade = "Capacity";

		for (String key : est.keySet()) {
			Estacao estacao = est.get(key);
			int nbikes = estacao.getnBikes();
			int capacity = estacao.getCapacidade();
			model.addValue(nbikes, nbicicletas, key);
			model.addValue(capacity, capacidade, key);
		}

	}

	public void execute(Map<String, Estacao> est) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				final String nbicicletas = "No. Bikes";
				final String capacidade = "Capacity";

				for (String key : est.keySet()) {
					Estacao estacao = est.get(key);
					int nbikes = estacao.getnBikes();
					int capacity = estacao.getCapacidade();
					model.setValue(nbikes, nbicicletas, key);
					model.setValue(capacity, capacidade, key);
				}
			}
		});
	}

	@Override
	public void run() {
	}
}