package gui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import javafx.util.Duration;
import javafx.animation.AnimationTimer;
import javafx.animation.FillTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.*;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import util.Coordenada;
import util.Estacao;

public class Mapa extends Application implements Runnable {

	private static int tempoActualizacao = 1000;
	private static Map<String, MapaObject> costumers = new HashMap<>();
	private static Map<String, MapaObject> estacoes = new HashMap<>();
	private static List<MapaObject> objectos = new ArrayList<>();

	private static Pane root = new Pane();
	private static Label labelUsers  = new Label("Users: ");
	private static Label labelEstacoes = new Label("Estacoes: ");

	@Override
	public void start(Stage stage) throws Exception {
		stage.setScene(new Scene(createContent()));
		stage.setTitle("InterfaceAgent - Mapa");
		stage.show();

	}

	private static Parent createContent() {

		root = new Pane();
		root.setPrefSize(Dimensions.GRID_WIDTH, Dimensions.GRID_HEIGHT);

		labelUsers.setFont(new Font(20));
		labelUsers.setLayoutX(10);
		labelUsers.setLayoutY(10);
		labelUsers.toFront();
		root.getChildren().add(labelUsers);

		labelEstacoes.setFont(new Font(20));
		labelEstacoes.setLayoutX(10);
		labelEstacoes.setLayoutY(35);
		labelEstacoes.toFront();
		root.getChildren().add(labelEstacoes);

		AnimationTimer timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				onUpdate();
			}
		};
		timer.start();
		return root;
	}

	public static void onUpdate() {
		objectos.forEach(MapaObject::update);
	}

	public static void update(Map<String, Coordenada> cos, Map<String, Estacao> est) {

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				

				// costumers
				for (String key : cos.keySet()) {
					CoordenadaMapa coordenada = new CoordenadaMapa(cos.get(key));
					if (costumers.containsKey(key)) {
						// update
						/*
						 * if (!costumers.get(key).coordenadaActual().equals(coordenada)) {
						 * costumers.get(key).setTransition(null, coordenada); EstacaoMapa object = new
						 * EstacaoMapa(coordenada, Color.AQUA);
						 * 
						 * addMapaObject(object, coordenada); System.out.println("azul: " + coordenada);
						 * 
						 * TranslateTransition transition = new TranslateTransition();
						 * transition.setToX(100); transition.setToY(100); transition.setNode(new
						 * Circle(0, 0, 5, Color.BLACK)); transition.setCycleCount(1);
						 * transition.setRate(2); transition.setDuration(Duration.seconds(0.5));
						 * transition.play(); }
						 * 
						 */
						/*
						 * if (!costumers.get(key).coordenadaActual().equals(coordenada)) { Circle cir =
						 * new Circle(); cir.setTranslateX(coordenada.getX());
						 * cir.setTranslateY(coordenada.getY());
						 * 
						 * costumers.get(key).setTransition(null, cir); }
						 */

						Circle cir = new Circle(coordenada.getX(), coordenada.getY(), 5);
						cir.setFill(Color.BLUE);

						costumers.get(key).setTransition(Duration.seconds(0.5), coordenada);
						/*
						 * removeMapaObject(costumers.get(key)); CostumerMapa object = new
						 * CostumerMapa(cir, coordenada); addMapaObject(object, coordenada);
						 * costumers.put(key, object);
						 */

					} else {
						// create
						Circle cir = new Circle();
						cir.setTranslateX(coordenada.getX());
						cir.setTranslateY(coordenada.getY());
						cir.setRadius(5);
						cir.setFill(Color.BLUE);

						CostumerMapa object = new CostumerMapa(cir, coordenada);
						addMapaObject(object, coordenada);
						costumers.put(key, object);
					}

				}

				// delete
				Iterator<String> it = costumers.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					if (!cos.containsKey(key)) {
						removeMapaObject(costumers.get(key));
						it.remove();
					}
				}

				// estacoes
				for (String key : est.keySet()) {
					Estacao estacao = est.get(key);
					CoordenadaMapa coordenada = new CoordenadaMapa(estacao.getCoordenada());
					Color corEstacao = getColorByRatio(estacao.getRatio());

					Circle cir = new Circle(15);
					cir.setFill(corEstacao);
					cir.setTranslateX(coordenada.getX());
					cir.setTranslateY(coordenada.getY());

					EstacaoMapa object = new EstacaoMapa(cir, coordenada);

					Circle cirRaio = new Circle(estacao.getRaio());
					cirRaio.setFill(Color.LIGHTYELLOW);
					cirRaio.setTranslateX(coordenada.getX());
					cirRaio.setTranslateY(coordenada.getY());
					cir.toBack();
					RaioEstacaoMapa raio = new RaioEstacaoMapa(cirRaio, coordenada);
					
					if (estacoes.containsKey(key)) {
						// update
						addMapaObject(object, coordenada);
						removeMapaObject(estacoes.get(key));
					} else {
						// create
						addMapaObject(object, coordenada);

						addMapaObject(raio, coordenada);
						estacoes.put(key, object);
					}

				}

				// delete
				Iterator<String> ite = estacoes.keySet().iterator();
				while (ite.hasNext()) {
					String key = ite.next();
					if (!est.containsKey(key)) {
						removeMapaObject(estacoes.get(key));
						ite.remove();
					}
				}
				
				
				labelUsers.setText("Users: " + cos.size());
				labelUsers.toFront();
				labelEstacoes.setText("Stations: " + est.size());
				labelEstacoes.toFront();
			}
			
			
		});

	}

	private static Color getColorByRatio(double ratio) {
		Color cor;
		if (ratio == 1) {
			cor = Color.RED;
		} else if (ratio >= 0.6) {
			cor = Color.ORANGE;
		} else if (ratio >= 0.3) {
			cor = Color.YELLOW;
		} else if (ratio > 0) {
			cor = Color.YELLOWGREEN;
		} else {
			cor = Color.GREEN;
		}
		return cor;
	}

	public static void addMapaObject(MapaObject object, CoordenadaMapa c) {
		object.getView().setTranslateX(c.getX());
		object.getView().setTranslateY(c.getY());
		root.getChildren().add(object.getView());
		objectos.add(object);
	}

	private static void removeMapaObject(MapaObject object) {
		root.getChildren().remove(object.getView());
		objectos.remove(object);
	}

	private static class EstacaoMapa extends MapaObject {
		public EstacaoMapa(Circle cir, CoordenadaMapa c) {
			super(cir, c);
		}
	}

	private static class RaioEstacaoMapa extends MapaObject {
		public RaioEstacaoMapa(Circle cir, CoordenadaMapa c) {
			super(cir, c);
		}
	}

	private static class CostumerMapa extends MapaObject {
		public CostumerMapa(Circle cir, CoordenadaMapa c) {
			super(cir, c);
		}
	}

	@Override
	public void run() {
		launch();
	}

	public static void setTempoUpdate(int tempo) {
		tempoActualizacao = tempo;

	}

}
