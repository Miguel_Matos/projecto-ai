package gui;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import util.Coordenada;

public class MapaObject {

	private Node view;

	private boolean alive = true;
	private Point2D velocity = new Point2D(0, 0);
	private CoordenadaMapa actual;

	public MapaObject(Node view, CoordenadaMapa a) {
		this.view = view;
		this.actual = a;
	}

	public void update() {
		//view.setTranslateX(view.getTranslateX() + velocity.getX());
		//view.setTranslateY(view.getTranslateY() + velocity.getY());
	}

	public void setTransition(Duration tempo, CoordenadaMapa c2) {
		
		TranslateTransition transition = new TranslateTransition();
        transition.setNode(view);
        transition.setToX(c2.getX());
        transition.setToY(c2.getY());
        transition.setDuration(tempo);
        transition.play();
        
	}

	public void setVelocity(double x, double y, CoordenadaMapa c) {

		x = x - view.getTranslateX();
		y = y - view.getTranslateX();

		double maior = Math.abs(x) > Math.abs(y) ? Math.abs(x) : Math.abs(y);

		x = x / maior;
		y = y / maior;

		this.velocity = new Point2D(x, y);
		actual=c;
	}

	public Point2D getVelocity() {
		return velocity;
	}

	public Node getView() {
		return view;
	}

	public boolean isAlive() {
		return alive;
	}

	public boolean isDead() {
		return !alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public double getRotate() {
		return view.getRotate();
	}

	public CoordenadaMapa coordenadaActual() {
		return actual;
	}

	public boolean isColliding(MapaObject other) {
		return getView().getBoundsInParent().intersects(other.getView().getBoundsInParent());
	}
}
