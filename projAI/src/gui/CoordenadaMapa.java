package gui;

import java.util.Locale;

import util.Coordenada;

public class CoordenadaMapa {

	private double x;
	private double y;

	public CoordenadaMapa(double x, double y) {
		this.x = x;
		this.y = y;
	}

	// assume que o maximo da coordenada vai ser x=100 e y=100
	public CoordenadaMapa(Coordenada c) {
		this.x = c.getX();
		this.y = c.getY();
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return String.format(Locale.US, "%.1f,%.1f", x, y);
	}

	@Override
	public boolean equals(Object obj) {
		CoordenadaMapa o = (CoordenadaMapa) obj;
		return x == o.getX() && y == o.getY();
	}
}
