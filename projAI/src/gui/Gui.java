package gui;

import java.util.Map;

import util.Coordenada;
import util.Estacao;

public class Gui extends Thread{
	
	@Override
	public void run() {
		Mapa.launch(Mapa.class);
	}
	public void update(Map<String, Coordenada> cos, Map<String, Estacao> est) {
		Mapa.update(cos,est);
	}
}
